{{ with $ip_address := (env "NOMAD_HOST_IP_app") }}
{{ with secret "pki_int/issue/cert" "role_name=restaurant-service" "common_name=restaurant-service.service.consul" "ttl=24h" "alt_names=_restaurant-service._tcp.service.consul,localhost" (printf "ip_sans=127.0.0.1,%s" $ip_address) }}
{{ .Data.certificate }}
{{ end }}{{ end }}
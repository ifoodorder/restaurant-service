# nginx config
HTTPS_PORT = ${NOMAD_PORT_app}
CERT_PATH = /local/cert.pem
CERT_KEY_PATH = /secrets/key.pem

# java config
JAVA_PORT = 8050
{{- with $instances := service "keycloak" }}
    {{- with $keycloak := index $instances 0 }}
KEYCLOAK_URL = "https://keycloak.service.consul:{{ $keycloak.Port }}/auth"
    {{- end }}
{{- end }}
KEYCLOAK_RESOURCE = Restaurant
KEYCLOAK_SECRET = {{ with secret "secret/restaurant/prod" }}{{ .Data.RESTAURANT_KEYSTORE_PWD }}{{ end }}
JDBC_URL = "jdbc:postgresql://postgresql.service.consul:5432/restaurant"
JDBC_USER = {{ with secret "/database/creds/restaurant" }}{{ .Data.username }}
JDBC_PWD = {{ .Data.password }}{{ end }}
H2_CONSOLE = false
RABBITMQ_HOST = "rabbitmq.service.consul"
RABBITMQ_PORT = "5671"
RABBITMQ_VIRTUALHOST = ifoodorder
SQL_INIT = never
LOG_FILE = logs/restaurant-service.log
GEO_IP_URL = http://api.ipstack.com/
GEO_IP_KEY = {{ with secret "secret/restaurant/prod" }}{{ .Data.GEO_IP_KEY }}{{ end }}
LOG_LEVEL = INFO
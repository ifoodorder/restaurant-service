package cz.ifoodorder.restaurantservice.geolocation;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import cz.ifoodorder.restaurantservice.config.IpStackProperties;
import cz.ifoodorder.restaurantservice.geolocation.pojo.IpStack;

@Service
@Qualifier(GeolocationServices.IP_STACK)
public class IpStackGeolocationService implements GeolocationService {
	private final IpStackProperties properties;
	private final WebClient client;

	public IpStackGeolocationService(IpStackProperties properties) {
		this.properties = properties;
		this.client = WebClient.builder().baseUrl(properties.getUrl()).build();
	}
	
	@Override
	public IpStack findByIp(String ip) {
		var x = client.get().uri(ip + "?access_key="+properties.getApiKey()+"&output=json").accept(MediaType.APPLICATION_JSON).retrieve().toEntity(IpStack.class);
		return x.block().getBody();
	}
	
}

package cz.ifoodorder.restaurantservice.geolocation.pojo;

import java.util.Collection;

import lombok.Data;

@Data
public class IpStackLocation {
	private String geonameId;
	private Collection<IpStackLanguage> languages;
	private String countryFlag;
	private String countryFlagEmoji;
	private String countryFlagEmojiUnicode;
	private String callingCode;
	private boolean isEu;
}

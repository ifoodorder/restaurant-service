package cz.ifoodorder.restaurantservice.geolocation.pojo;

import lombok.Data;

@Data
public class IpStackCurrency {
	private String code;
	private String name;
	private String plural;
	private String symbol;
	private String symbolNative;
}

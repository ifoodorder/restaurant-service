package cz.ifoodorder.restaurantservice.geolocation.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class IpStackLanguage {
	private String code;
	private String name;
	@JsonProperty("native")
	private String nativeName;
}

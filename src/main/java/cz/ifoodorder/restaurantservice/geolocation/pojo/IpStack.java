package cz.ifoodorder.restaurantservice.geolocation.pojo;

import lombok.Data;

@Data
public class IpStack implements Geolocation {
	private String ip;
	private String hostname;
	private String type;
	private String continentCode;
	private String continentName;
	private String countryCode;
	private String countryName;
	private String regionCode;
	private String regionName;
	private String city;
	private String zip;
	private String latitude;
	private String longtitude;
	private IpStackLocation location;
	private IpStackTimeZone timeZone;
	private IpStackCurrency currency;
	private IpStackConnection connection;
	private IpStackSecurity security;
	
	
}

package cz.ifoodorder.restaurantservice.geolocation.pojo;

import java.util.Collection;

import lombok.Data;

@Data
public class IpStackSecurity {
	private boolean isProxy;
	private String proxyType;
	private boolean isCrawler;
	private String crawlerName;
	private String crawlerType;
	private boolean isTor;
	private String threatLevel;
	private Collection<String> threatTypes;
}

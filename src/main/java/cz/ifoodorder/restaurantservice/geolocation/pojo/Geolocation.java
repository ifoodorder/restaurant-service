package cz.ifoodorder.restaurantservice.geolocation.pojo;

public interface Geolocation {
	String getCity();
	String getCountryName();
	String getCountryCode();
}

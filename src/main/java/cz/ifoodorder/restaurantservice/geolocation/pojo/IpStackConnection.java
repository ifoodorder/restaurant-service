package cz.ifoodorder.restaurantservice.geolocation.pojo;

import lombok.Data;

@Data
public class IpStackConnection {
	private String asn;
	private String isp;
}

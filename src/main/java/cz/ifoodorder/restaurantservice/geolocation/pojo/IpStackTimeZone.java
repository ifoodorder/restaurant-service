package cz.ifoodorder.restaurantservice.geolocation.pojo;

import java.util.Date;

import lombok.Data;

@Data
public class IpStackTimeZone {
	private String id;
	private Date currentTime;
	private String gmtOffset;
	private String code;
	private boolean isDaylightSaving;
}

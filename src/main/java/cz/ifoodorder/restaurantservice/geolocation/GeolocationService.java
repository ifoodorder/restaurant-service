package cz.ifoodorder.restaurantservice.geolocation;

import cz.ifoodorder.restaurantservice.geolocation.pojo.Geolocation;

public interface GeolocationService {
	Geolocation findByIp(String ip);
}

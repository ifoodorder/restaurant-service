package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.Data;

@Data
@Entity(name = "restaurant")
public class Restaurant {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	@ManyToOne(cascade = CascadeType.ALL)
	private Company company;
	private String name;
	@ManyToOne(cascade = CascadeType.ALL)
	private Address address;
	private String note;
	private String logo;
	private boolean visible;
	@ManyToOne(cascade = CascadeType.ALL)
	private Language language;
	@ManyToOne(cascade = CascadeType.ALL)
	private Currency currency;
	@OneToOne(cascade = CascadeType.ALL)
	private DeliverySettings deliverySettings;
	@OneToOne(cascade = CascadeType.ALL)
	private PaymentSettings paymentSettings;
	@OneToOne(cascade = CascadeType.ALL)
	private OpeningHoursSettings openingHoursSettings;
}

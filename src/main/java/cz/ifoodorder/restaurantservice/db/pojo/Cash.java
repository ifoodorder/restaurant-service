package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.Collection;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import lombok.Data;

@Data
@Entity(name = "cash")
public class Cash {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private boolean enabled;
	private boolean allowUnregistered;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "currency_cash_xref",
		joinColumns = @JoinColumn(name = "cash_id", referencedColumnName = "id"),
		inverseJoinColumns = @JoinColumn(name = "currency_id", referencedColumnName = "id")
	)
	private Collection<Currency> currencies;
}

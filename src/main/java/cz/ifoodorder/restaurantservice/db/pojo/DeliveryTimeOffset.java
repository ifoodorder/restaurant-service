package cz.ifoodorder.restaurantservice.db.pojo;

import java.time.Duration;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name = "delivery_time_offset")
public class DeliveryTimeOffset {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private Duration timeOffset;
	private String formula;
}

package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity(name = "fee")
public class Fee {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	@ManyToOne(cascade = CascadeType.ALL)
	private FeeType feeType;
	private String formula;
	private boolean enabled;
}

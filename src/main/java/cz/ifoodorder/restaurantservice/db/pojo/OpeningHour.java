package cz.ifoodorder.restaurantservice.db.pojo;

import java.time.LocalTime;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity(name = "opening_hours")
public class OpeningHour {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private boolean enabled;
	private LocalTime from;
	private LocalTime to;
	@ManyToOne(targetEntity = OpeningHoursSettings.class)
	@JoinColumn(name = "opening_hours_settings.id")
	private OpeningHoursSettings openingHoursSettings;
	@ManyToOne
	private Day day;
}

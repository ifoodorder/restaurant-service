package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity(name = "company")
public class Company {
	@Id
	private UUID id;
	@ManyToOne
	private Address address;
	private String companyName;
	private String legalEntityNumber;
	private String taxId;
	@OneToMany(mappedBy = "company")
	@JsonIgnore
	private List<Restaurant> restaurants;
}

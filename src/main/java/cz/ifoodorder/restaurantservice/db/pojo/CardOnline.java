package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name = "card_online")
public class CardOnline {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private boolean enabled;
}

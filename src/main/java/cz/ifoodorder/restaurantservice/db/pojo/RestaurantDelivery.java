package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.Collection;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity(name = "restaurant_delivery")
public class RestaurantDelivery {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private boolean enabled;
	private String note;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "restaurant_delivery_fee_xref",
		joinColumns = @JoinColumn(name = "restaurant_delivery_id", referencedColumnName = "id"),
		inverseJoinColumns = @JoinColumn(name = "fee_id", referencedColumnName = "id"))
	private Collection<Fee> fees;
	@ManyToOne(cascade = CascadeType.ALL)
	private DeliveryTimeSettings deliveryTimeSettings;
}

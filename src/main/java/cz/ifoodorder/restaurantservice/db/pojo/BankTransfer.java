package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name = "bank_transfer")
public class BankTransfer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private String bankAccountNumber;
	private String additionalInfo;
	private boolean allowUnregistered;
}

package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.Collection;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity(name = "card_on_delivery")
public class CardOnDelivery {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private boolean enabled;
	private boolean allowUnregistered;
	@OneToMany
	@JoinTable(name = "card_on_delivery_card_xref",
			joinColumns = @JoinColumn(name = "card_on_delivery_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "card_id", referencedColumnName = "id"))
	private Collection<CardType> cards;
}

package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.Collection;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import org.springframework.data.geo.Polygon;

import lombok.Data;

@Data
@Entity(name = "courier_delivery")
public class CourierDelivery {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private boolean enabled;
	private String note;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "courier_delivery_fee_xref",
		joinColumns = @JoinColumn(name = "courier_delivery_id", referencedColumnName = "id"),
		inverseJoinColumns = @JoinColumn(name = "fee_id", referencedColumnName = "id"))
	private Collection<Fee> fees;
	private Polygon area;
	private Integer maxDistance;
	@OneToOne(cascade = CascadeType.ALL)
	private DeliveryTimeSettings deliveryTimeSettings;
}

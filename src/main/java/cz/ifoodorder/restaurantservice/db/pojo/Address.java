package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity(name = "address")
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private String street;
	private String zip;
	private String city;
	@ManyToOne
	private Country country;
}

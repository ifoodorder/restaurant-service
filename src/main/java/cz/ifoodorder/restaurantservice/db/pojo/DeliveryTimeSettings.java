package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity(name = "delivery_time_settings")
public class DeliveryTimeSettings {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	@ManyToOne(cascade = CascadeType.ALL)
	private PreorderConstraint preorderConstraint;
	@ManyToOne
	private DeliveryTimeOffset deliveryTimeOffset;
}

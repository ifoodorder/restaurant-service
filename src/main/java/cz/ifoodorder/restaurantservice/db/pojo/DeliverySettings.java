package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity(name = "delivery_settings")
public class DeliverySettings {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	@ManyToOne(cascade = CascadeType.ALL)
	private RestaurantDelivery restaurantDelivery;
	@ManyToOne(cascade = CascadeType.ALL)
	private CourierDelivery courierDelivery;
	@ManyToOne(cascade = CascadeType.ALL)
	private PickupPointDelivery pickupPointDelivery;
}

package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity(name = "pickup_point")
public class PickupPoint {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private String name;
	@ManyToOne(cascade = CascadeType.ALL)
	private Address address;
	@ManyToOne
	@JoinColumn(name = "pickup_point_delivery_id")
	private PickupPointDelivery pickupPointDelivery;
}

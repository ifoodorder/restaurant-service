package cz.ifoodorder.restaurantservice.db.pojo;

import java.time.Duration;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name = "preorder_constraint")
public class PreorderConstraint {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private boolean enabled;
	private Duration availableAfter;
	private Duration availableBefore;
	private Duration availableInterval;
}

package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.Collection;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity(name = "opening_hours_settings")
public class OpeningHoursSettings {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private String note;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "openingHoursSettings")
	private Collection<OpeningHour> openingHours;
}

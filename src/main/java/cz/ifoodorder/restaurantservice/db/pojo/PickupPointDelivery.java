package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.Collection;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity(name = "pickup_point_delivery")
public class PickupPointDelivery {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private boolean enabled;
	private String note;
	@ManyToOne(cascade = CascadeType.ALL)
	private DeliveryTimeSettings deliveryTimeSettings;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "pickupPointDelivery")
	private Collection<PickupPoint> pickupPoints;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "pickup_point_delivery_fee_xref",
		joinColumns = @JoinColumn(name = "pickup_point_delivery_id", referencedColumnName = "id"),
		inverseJoinColumns = @JoinColumn(name = "fee_id", referencedColumnName = "id"))
	private Collection<Fee> fees;
}

package cz.ifoodorder.restaurantservice.db.pojo;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity(name = "payment_settings")
public class PaymentSettings {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	@ManyToOne(cascade = CascadeType.ALL)
	private Cash cash;
	@ManyToOne(cascade = CascadeType.ALL)
	private BankTransfer bankTransfer;
	@ManyToOne(cascade = CascadeType.ALL)
	private CardOnDelivery cardOnDelivery;
	@ManyToOne(cascade = CascadeType.ALL)
	private CardOnline cardOnline;
	@ManyToOne(cascade = CascadeType.ALL)
	private Bitcoin bitcoin;
	@ManyToOne(cascade = CascadeType.ALL)
	private MealVoucher mealVoucher;
}

package cz.ifoodorder.restaurantservice.db.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import cz.ifoodorder.restaurantservice.db.pojo.Restaurant;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RestaurantSpecification implements Specification<Restaurant> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -495694519002134126L;
	private final RestaurantFilter filter;
	private final UUID ownerGroupId;
	public RestaurantSpecification(RestaurantFilter filter) {
		this(filter, null);
	}
	public RestaurantSpecification(RestaurantFilter filter, UUID ownerGroupId) {
		this.filter = filter;
		this.ownerGroupId = ownerGroupId;
	}
	@Override
	public Predicate toPredicate(Root<Restaurant> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate> predicates = new ArrayList<>();

		if(filter == null) {
			return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
		}
		if(filter.getRestaurantName() != null && !filter.getRestaurantName().isEmpty()) {
			predicates.add(criteriaBuilder.equal(root.get("name"), filter.getRestaurantName()));
		}
		if(filter.getCountry() != null && !filter.getCountry().isEmpty()) {
			predicates.add(criteriaBuilder.equal(root.get("address").get("country").get("name"), filter.getCountry()));
		}
		if(filter.getCity() != null && !filter.getCity().isEmpty()) {
			predicates.add(criteriaBuilder.like(root.get("address").get("city"), filter.getCity() + "%"));
		}
		if(filter.getDelivery() != null && filter.getDelivery().isEmpty()) {
			predicates.add(criteriaBuilder.like(root.get("address.city"), filter.getCity()));
		}
		if(filter.getFulltext() != null && !filter.getFulltext().isEmpty()) {
			// TODO
		}
		 
		log.info("ownerGroupId: {}, filterIsOwning: {}", ownerGroupId, filter.isOwningOnly());
		if(ownerGroupId != null && filter.isOwningOnly()) {
			predicates.add(criteriaBuilder.equal(root.get("company").get("id"), ownerGroupId));
		}
		else {
			predicates.add(criteriaBuilder.isTrue(root.get("visible")));
		}
		
		return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
	}

}

package cz.ifoodorder.restaurantservice.db.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import lombok.AllArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestaurantFilter{
	private String country;
	private String city;
	private List<String> delivery;
	private List<String> category;
	private String fulltext;
	private String restaurantName;
	private boolean hidden;
	private boolean owningOnly;
}

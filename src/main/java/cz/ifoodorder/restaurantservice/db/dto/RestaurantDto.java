package cz.ifoodorder.restaurantservice.db.dto;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import cz.ifoodorder.restaurantservice.db.pojo.Restaurant;


public interface RestaurantDto extends PagingAndSortingRepository<Restaurant, UUID>, JpaSpecificationExecutor<Restaurant> {
	Optional<Restaurant> findOneByIdAndCompanyId(UUID id, UUID companyId);

	List<Restaurant> findByCompanyId(UUID id);
}

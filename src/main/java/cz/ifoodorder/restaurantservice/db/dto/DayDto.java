package cz.ifoodorder.restaurantservice.db.dto;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.restaurantservice.db.pojo.Day;



public interface DayDto extends JpaRepository<Day, UUID> {
	Optional<Day> findOneByName(String name);
	
}

package cz.ifoodorder.restaurantservice.db.dto;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.restaurantservice.db.pojo.Address;

public interface AddressDto extends JpaRepository<Address, UUID> {

}

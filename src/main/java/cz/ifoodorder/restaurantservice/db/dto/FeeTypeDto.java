package cz.ifoodorder.restaurantservice.db.dto;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.restaurantservice.db.pojo.FeeType;

public interface FeeTypeDto extends JpaRepository<FeeType, UUID> {
	Optional<FeeType> findOneByName(String name);
	
}

package cz.ifoodorder.restaurantservice.db.dto;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.restaurantservice.db.pojo.Language;



public interface LanguageDto extends JpaRepository<Language, UUID> {
	Optional<Language> findOneByName(String name);
	
}

package cz.ifoodorder.restaurantservice.db.dto;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.restaurantservice.db.pojo.CardType;

public interface CardTypeDto extends JpaRepository<CardType, UUID> {
	Optional<CardType> findOneByName(String name);
	
}

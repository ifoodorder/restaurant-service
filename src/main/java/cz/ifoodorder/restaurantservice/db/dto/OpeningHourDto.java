package cz.ifoodorder.restaurantservice.db.dto;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.restaurantservice.db.pojo.OpeningHour;



public interface OpeningHourDto extends JpaRepository<OpeningHour, UUID> {
	
}

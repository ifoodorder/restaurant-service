package cz.ifoodorder.restaurantservice.db.dto;

import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import cz.ifoodorder.restaurantservice.db.pojo.Company;

public interface CompanyDto extends CrudRepository<Company, UUID> {

}

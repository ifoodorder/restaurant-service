package cz.ifoodorder.restaurantservice.db.dto;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.restaurantservice.db.pojo.Country;



public interface CountryDto extends JpaRepository<Country, UUID> {
	Optional<Country> findOneByName(String name);
	
}

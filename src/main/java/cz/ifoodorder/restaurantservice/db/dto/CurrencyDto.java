package cz.ifoodorder.restaurantservice.db.dto;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.restaurantservice.db.pojo.Currency;

public interface CurrencyDto extends JpaRepository<Currency, UUID> {
	Optional<Currency> findOneByName(String name);
}

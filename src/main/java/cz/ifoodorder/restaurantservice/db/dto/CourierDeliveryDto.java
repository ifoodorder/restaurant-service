package cz.ifoodorder.restaurantservice.db.dto;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import cz.ifoodorder.restaurantservice.db.pojo.CourierDelivery;

public interface CourierDeliveryDto extends CrudRepository<CourierDelivery, UUID> {

}

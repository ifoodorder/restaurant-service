package cz.ifoodorder.restaurantservice.ampq;

import java.util.UUID;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

import cz.ifoodorder.restaurantservice.db.dto.CompanyDto;
import cz.ifoodorder.restaurantservice.db.pojo.Company;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RabbitListener(queues = "company")
public class CompanyMQReceiver {
	private final CompanyDto companyDto;
	
	public CompanyMQReceiver(CompanyDto companyDto) {
		this.companyDto = companyDto;
	}
	
	@RabbitHandler
	public void receive(UUID companyId) {
		log.info("Received: {}", companyId);
		
		var company = new Company();
		company.setId(companyId);
		companyDto.save(company);
	}

}

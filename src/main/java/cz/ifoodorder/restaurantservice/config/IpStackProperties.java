package cz.ifoodorder.restaurantservice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;
import lombok.ToString;

@Data @ToString
@Configuration
@ConfigurationProperties(prefix = "ifoodorder.geolocation.ip-stack")
public class IpStackProperties {
	private String apiKey;
	private String apiKeyPathVariable;
	private String url;
}

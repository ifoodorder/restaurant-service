package cz.ifoodorder.restaurantservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cz.ifoodorder.restaurantservice.ampq.CompanyMQReceiver;
import cz.ifoodorder.restaurantservice.db.dto.CompanyDto;

@Configuration
public class RabbitMqConfig {
	private final CompanyDto companyDto;
	
	RabbitMqConfig(CompanyDto companyDto) {
		this.companyDto = companyDto;
	}
	
	@Bean
	public CompanyMQReceiver company() {
		return new CompanyMQReceiver(companyDto);
	}
}

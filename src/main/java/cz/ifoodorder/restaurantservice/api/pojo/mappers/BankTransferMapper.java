package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiBankTransfer;
import cz.ifoodorder.restaurantservice.db.pojo.BankTransfer;

@Component
public class BankTransferMapper {
	public BankTransfer mapToDb(ApiBankTransfer apiBankTransfer) {
		if(apiBankTransfer == null) {
			return null;
		}
		var bankTransfer = new BankTransfer();
		bankTransfer.setAdditionalInfo(apiBankTransfer.getAdditionalInfo());
		bankTransfer.setAllowUnregistered(apiBankTransfer.isAllowUnregistered());
		bankTransfer.setBankAccountNumber(apiBankTransfer.getBankAccountNumber());
		bankTransfer.setId(apiBankTransfer.getId());
		return bankTransfer;
	}
	
	public ApiBankTransfer mapToApi(BankTransfer bankTransfer) {
		if(bankTransfer == null) {
			return null;
		}
		var apiBankTransfer = new ApiBankTransfer();
		apiBankTransfer.setAdditionalInfo(bankTransfer.getAdditionalInfo());
		apiBankTransfer.setAllowUnregistered(bankTransfer.isAllowUnregistered());
		apiBankTransfer.setBankAccountNumber(bankTransfer.getBankAccountNumber());
		apiBankTransfer.setId(bankTransfer.getId());
		return apiBankTransfer;
	}
}

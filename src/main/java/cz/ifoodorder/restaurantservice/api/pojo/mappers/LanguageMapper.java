package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiLanguage;
import cz.ifoodorder.restaurantservice.db.dto.LanguageDto;
import cz.ifoodorder.restaurantservice.db.pojo.Language;

@Component
public class LanguageMapper {
	private final LanguageDto dto;
	public LanguageMapper(LanguageDto dto) {
		this.dto = dto;
	}
	
	public Language mapToDb(ApiLanguage apiLanguage) {
		if(apiLanguage == null) {
			return null;
		}
		return dto.findOneByName(apiLanguage.getName()).orElseThrow();
	}
	
	public ApiLanguage mapToApi(Language language) {
		if(language == null) {
			return null;
		}
		return new ApiLanguage(language.getName());
	}
	
	public Collection<ApiLanguage> mapToApi(Collection<Language> days) {
		return days.stream().map(this::mapToApi).collect(Collectors.toList());
	}
}

package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiAddress;
import cz.ifoodorder.restaurantservice.db.pojo.Address;

@Component
public class AddressMapper {
	private CountryMapper countryMapper;
	AddressMapper(CountryMapper countryMapper) {
		this.countryMapper = countryMapper;
	}

	public Address mapToDb(ApiAddress viewAddress) {
		if(viewAddress == null) {
			return null;
		}
		var address = new Address();
		address.setCity(viewAddress.getCity());
		address.setCountry(countryMapper.mapToDb(viewAddress.getCountry()));
		address.setZip(viewAddress.getZip());
		address.setId(null);
		address.setStreet(viewAddress.getStreet());
		return address;
	}

	public ApiAddress mapToApi(Address address) {
		if(address == null) {
			return null;
		}
		var apiAddress = new ApiAddress();
		apiAddress.setBuilding(null);
		apiAddress.setCity(address.getCity());
		apiAddress.setCountry(countryMapper.mapToApi(address.getCountry()));
		apiAddress.setZip(address.getZip());
		apiAddress.setStreet(address.getStreet());
		return apiAddress;
	}
}

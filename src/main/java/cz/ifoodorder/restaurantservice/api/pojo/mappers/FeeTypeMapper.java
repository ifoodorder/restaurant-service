package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiFeeType;
import cz.ifoodorder.restaurantservice.db.dto.FeeTypeDto;
import cz.ifoodorder.restaurantservice.db.pojo.FeeType;

@Component
public class FeeTypeMapper {
	private FeeTypeDto dto;
	public FeeTypeMapper(FeeTypeDto dto) {
		this.dto = dto;
	}
	
	public FeeType mapToDb(ApiFeeType apiFeeType) {
		if(apiFeeType == null) {
			return null;
		}
		return dto.findOneByName(apiFeeType.getName()).orElseThrow();
	}

	public ApiFeeType mapToApi(FeeType feeType) {
		if(feeType == null) {
			return null;
		}
		var viewFeeType = new ApiFeeType();
		viewFeeType.setName(feeType.getName());
		return viewFeeType;
	}
	
	public Collection<ApiFeeType> mapToApi(Collection<FeeType> feeType) {
		return feeType.stream().map(this::mapToApi).collect(Collectors.toList());
	}
	
	public Collection<FeeType> mapToDb(Collection<ApiFeeType> viewFeeType) {
		return viewFeeType.stream().map(this::mapToDb).collect(Collectors.toList());
	}
}

package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiRestaurantDelivery;
import cz.ifoodorder.restaurantservice.db.pojo.RestaurantDelivery;

@Component
public class RestaurantDeliveryMapper {
	private final FeeMapper feeMapper;
	public RestaurantDeliveryMapper(FeeMapper feeMapper) {
		this.feeMapper = feeMapper;
	}
	
	public RestaurantDelivery mapToDb(ApiRestaurantDelivery apiRestaurantDelivery) {
		if(apiRestaurantDelivery == null) {
			return null;
		}
		var restaurantDelivery = new RestaurantDelivery();
		restaurantDelivery.setId(apiRestaurantDelivery.getId());
		restaurantDelivery.setEnabled(apiRestaurantDelivery.isEnabled());
		restaurantDelivery.setNote(apiRestaurantDelivery.getNote());
		restaurantDelivery.setDeliveryTimeSettings(null);
		return restaurantDelivery;
	}
	
	public ApiRestaurantDelivery mapToApi(RestaurantDelivery restaurantDelivery) {
		if(restaurantDelivery == null) {
			return null;
		}
		var apiRestaurantDelivery = new ApiRestaurantDelivery();
		apiRestaurantDelivery.setId(restaurantDelivery.getId());
		apiRestaurantDelivery.setEnabled(restaurantDelivery.isEnabled());
		apiRestaurantDelivery.setFees(feeMapper.mapToApi(restaurantDelivery.getFees()));
		apiRestaurantDelivery.setNote(restaurantDelivery.getNote());
		return apiRestaurantDelivery;
	}
}

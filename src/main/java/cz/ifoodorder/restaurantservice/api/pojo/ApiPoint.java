package cz.ifoodorder.restaurantservice.api.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiPoint {
	public ApiPoint(double x, double y) {
		this.x = Double.toString(x);
		this.y = Double.toString(y);
	}
	private String x;
	private String y;
}

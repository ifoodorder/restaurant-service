package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import cz.ifoodorder.restaurantservice.api.pojo.ApiRestaurant;
import cz.ifoodorder.restaurantservice.db.pojo.Restaurant;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

@Component
public class RestaurantMapper {
	private DeliverySettingsMapper deliverySettingsMapper;
	private AddressMapper addressMapper;
	private CurrencyMapper currencyMapper;
	private OpeningHoursSettingsMapper openingHoursSettingsMapper;
	private PaymentSettingsMapper paymentSettingsMapper;
	private LanguageMapper languageMapper;

	public RestaurantMapper(AddressMapper addressMapper,
							CurrencyMapper currencyMapper,
							DeliverySettingsMapper deliverySettingsMapper,
							OpeningHoursSettingsMapper openingHoursSettingsMapper,
							PaymentSettingsMapper paymentSettingsMapper,
							LanguageMapper languageMapper) {
		this.addressMapper = addressMapper;
		this.currencyMapper = currencyMapper;
		this.deliverySettingsMapper = deliverySettingsMapper;
		this.openingHoursSettingsMapper = openingHoursSettingsMapper;
		this.paymentSettingsMapper = paymentSettingsMapper;
		this.languageMapper = languageMapper;
	}

	public Restaurant mapToDb(ApiRestaurant apiRestaurant) {
		if(apiRestaurant == null) {
			return null;
		}
		Restaurant restaurant = new Restaurant();
		restaurant.setAddress(addressMapper.mapToDb(apiRestaurant.getAddress()));
//		r.setCompany(null);
		restaurant.setCurrency(currencyMapper.mapToDb(apiRestaurant.getCurrency()));
		restaurant.setDeliverySettings(deliverySettingsMapper.mapToDb(apiRestaurant.getDeliverySettings()));
		restaurant.setId(apiRestaurant.getId());
		restaurant.setLanguage(languageMapper.mapToDb(apiRestaurant.getLanguage()));
		restaurant.setLogo(apiRestaurant.getLogo());
		restaurant.setName(apiRestaurant.getName());
		restaurant.setNote(apiRestaurant.getNote());
		restaurant.setOpeningHoursSettings(openingHoursSettingsMapper.mapToDb(apiRestaurant.getOpeningHoursSettings()));
		restaurant.setVisible(apiRestaurant.isVisible());
		restaurant.setPaymentSettings(paymentSettingsMapper.mapToDb(apiRestaurant.getPaymentSettings()));
		
		return restaurant;
	}

	public ApiRestaurant mapToApi (Restaurant restaurant) {
		if(restaurant == null) {
			return null;
		}
		var apiRestaurant = new ApiRestaurant();
		apiRestaurant.setId(restaurant.getId());
		apiRestaurant.setAddress(addressMapper.mapToApi(restaurant.getAddress()));
		apiRestaurant.setCurrency(currencyMapper.mapToApi(restaurant.getCurrency()));
		apiRestaurant.setDeliverySettings(deliverySettingsMapper.mapToApi(restaurant.getDeliverySettings()));
		apiRestaurant.setLanguage(languageMapper.mapToApi(restaurant.getLanguage()));
		apiRestaurant.setLogo(restaurant.getLogo());
		apiRestaurant.setName(restaurant.getName());
		apiRestaurant.setNote(restaurant.getNote());
		apiRestaurant.setOpeningHoursSettings(openingHoursSettingsMapper.mapToApi(restaurant.getOpeningHoursSettings()));
		apiRestaurant.setPaymentSettings(paymentSettingsMapper.mapToApi(restaurant.getPaymentSettings()));
		apiRestaurant.setVisible(restaurant.isVisible());
		return apiRestaurant;
	}
	
	public List<ApiRestaurant> mapToApi (List<Restaurant> restaurants) {
		return restaurants.stream().map(this::mapToApi).collect(Collectors.toList());
	}
}

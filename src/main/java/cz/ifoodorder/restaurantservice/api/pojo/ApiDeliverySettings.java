package cz.ifoodorder.restaurantservice.api.pojo;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiDeliverySettings {
	private UUID id;
	private ApiRestaurantDelivery restaurantDelivery;
	private ApiCourierDelivery courierDelivery;
	private ApiPickupPointDelivery pickupPointDelivery;
}

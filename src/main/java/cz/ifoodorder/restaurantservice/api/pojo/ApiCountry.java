package cz.ifoodorder.restaurantservice.api.pojo;

import com.fasterxml.jackson.annotation.JsonValue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiCountry {
	
	@JsonValue
	private String name;
}

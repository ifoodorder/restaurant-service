package cz.ifoodorder.restaurantservice.api.pojo;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiRestaurant {
	private UUID id;
	private String name;
	private ApiAddress address;
	private String note;
	private String logo;
	private boolean visible;
	private ApiLanguage language;
	private ApiCurrency currency;
	private ApiDeliverySettings deliverySettings;
	private ApiPaymentSettings paymentSettings;
	private ApiOpeningHoursSettings openingHoursSettings;
}

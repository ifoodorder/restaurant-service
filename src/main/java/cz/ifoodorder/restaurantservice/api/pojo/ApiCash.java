package cz.ifoodorder.restaurantservice.api.pojo;

import java.util.Collection;
import java.util.UUID;

import lombok.Data;

@Data
public class ApiCash {
	private UUID id;
	private boolean enabled;
	private boolean allowUnregistered;
	private Collection<ApiCurrency> currencies;
}

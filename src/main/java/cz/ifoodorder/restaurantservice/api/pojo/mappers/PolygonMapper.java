package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import org.springframework.data.geo.Point;
import org.springframework.data.geo.Polygon;
import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiPoint;

@Component
public class PolygonMapper {
	public Polygon mapToDb(Collection<ApiPoint> apiPoints) {
		if (apiPoints == null) {
			return null;
		}
		var points = apiPoints.stream()
				.map(x -> new Point(Double.parseDouble(x.getX()), Double.parseDouble(x.getY())))
				.collect(Collectors.toList());
		return new Polygon(points);
	}
	
	public Collection<ApiPoint> mapToApi(Polygon polygon) {
		if (polygon == null) {
			return Collections.emptyList();
		}
		return polygon.getPoints().stream()
				.map(x -> new ApiPoint(x.getX(), x.getY()))
				.collect(Collectors.toList());
	}
}

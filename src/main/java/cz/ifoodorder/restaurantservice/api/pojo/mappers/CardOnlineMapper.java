package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiCardOnline;
import cz.ifoodorder.restaurantservice.db.pojo.CardOnline;

@Component
public class CardOnlineMapper {
	public CardOnline mapToDb(ApiCardOnline apiCardOnline) {
		if(apiCardOnline == null) {
			return null;
		}
		var cardOnline = new CardOnline();
		cardOnline.setEnabled(apiCardOnline.isEnabled());
		cardOnline.setId(apiCardOnline.getId());
		return cardOnline;
	}
	
	public ApiCardOnline mapToApi(CardOnline cardOnline) {
		if(cardOnline == null) {
			return null;
		}
		var apiCardOnline = new ApiCardOnline();
		apiCardOnline.setEnabled(cardOnline.isEnabled());
		apiCardOnline.setId(cardOnline.getId());
		return apiCardOnline;
	}
}

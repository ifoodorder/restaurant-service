package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiOpeningHoursSettings;
import cz.ifoodorder.restaurantservice.db.pojo.OpeningHoursSettings;

@Component
public class OpeningHoursSettingsMapper {
	private final OpeningHourMapper hourMapper;
	
	public OpeningHoursSettingsMapper(OpeningHourMapper hourMapper) {
		this.hourMapper = hourMapper;
	}
	
	public OpeningHoursSettings mapToDb(ApiOpeningHoursSettings apiOpeningHoursSettings) {
		if(apiOpeningHoursSettings == null) {
			return null;
		}
		var openingHoursSettings = new OpeningHoursSettings();
		openingHoursSettings.setId(apiOpeningHoursSettings.getId());
		openingHoursSettings.setNote(apiOpeningHoursSettings.getNote());
		openingHoursSettings.setOpeningHours(hourMapper.mapToDb(apiOpeningHoursSettings.getOpeningHours(), openingHoursSettings));
		return openingHoursSettings;
	}
	
	public ApiOpeningHoursSettings mapToApi(OpeningHoursSettings openingHoursSettings) {
		if(openingHoursSettings == null) {
			return null;
		}
		var apiOpeningHoursSettings = new ApiOpeningHoursSettings();
		apiOpeningHoursSettings.setId(openingHoursSettings.getId());
		apiOpeningHoursSettings.setNote(openingHoursSettings.getNote());
		apiOpeningHoursSettings.setOpeningHours(hourMapper.mapToApi(openingHoursSettings.getOpeningHours()));
		return apiOpeningHoursSettings;
	}
	
	public Collection<ApiOpeningHoursSettings> mapToApi(Collection<OpeningHoursSettings> openingHoursSettings) {
		return openingHoursSettings.stream().map(this::mapToApi).collect(Collectors.toList());
	}
}

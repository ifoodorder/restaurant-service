package cz.ifoodorder.restaurantservice.api.pojo;

import java.util.Collection;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiOpeningHoursSettings {
	private UUID id;
	private String note;
	private Collection<ApiOpeningHour> openingHours;
}

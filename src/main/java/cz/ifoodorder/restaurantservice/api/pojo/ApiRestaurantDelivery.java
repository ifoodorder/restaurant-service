package cz.ifoodorder.restaurantservice.api.pojo;

import java.util.Collection;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiRestaurantDelivery {
	private UUID id;
	private boolean enabled;
	private String note;
	private Collection<ApiFee> fees;
}

package cz.ifoodorder.restaurantservice.api.pojo;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiFee {
	private UUID id;
	private ApiFeeType feeType;
	private String formula;
	private boolean enabled;
}

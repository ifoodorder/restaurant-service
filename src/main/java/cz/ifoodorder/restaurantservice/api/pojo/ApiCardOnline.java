package cz.ifoodorder.restaurantservice.api.pojo;

import java.util.UUID;

import lombok.Data;

@Data
public class ApiCardOnline {
	private UUID id;
	private boolean enabled;
}

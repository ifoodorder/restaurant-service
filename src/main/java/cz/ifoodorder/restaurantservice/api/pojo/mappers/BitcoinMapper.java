package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiBitcoin;
import cz.ifoodorder.restaurantservice.db.pojo.Bitcoin;

@Component
public class BitcoinMapper {
	public Bitcoin mapToDb (ApiBitcoin apiBitcoin) {
		if(apiBitcoin == null) {
			return null;
		}
		var bitcoin = new Bitcoin();
		bitcoin.setAddress(apiBitcoin.getAddress());
		bitcoin.setEnabled(bitcoin.isEnabled());
		bitcoin.setId(apiBitcoin.getId());
		return bitcoin;
	}
	
	public ApiBitcoin mapToApi (Bitcoin bitcoin) {
		if(bitcoin == null) {
			return null;
		}
		var apiBitcoin = new ApiBitcoin();
		apiBitcoin.setAddress(bitcoin.getAddress());
		apiBitcoin.setEnabled(bitcoin.isEnabled());
		apiBitcoin.setId(bitcoin.getId());
		return apiBitcoin;
	}

}

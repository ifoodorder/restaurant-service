package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiPickupPoint;
import cz.ifoodorder.restaurantservice.db.pojo.PickupPoint;

@Component
public class PickupPointMapper {
	private final AddressMapper addressMapper;
	
	public PickupPointMapper(AddressMapper addressMapper) {
		this.addressMapper = addressMapper;
	}
	
	public PickupPoint mapToDb (ApiPickupPoint apiPickupPoint) {
		if(apiPickupPoint == null) {
			return null;
		}
		var pickupPoint = new PickupPoint();
		pickupPoint.setAddress(addressMapper.mapToDb(apiPickupPoint.getAddress()));
		pickupPoint.setId(apiPickupPoint.getId());
		pickupPoint.setName(apiPickupPoint.getName());
		pickupPoint.setPickupPointDelivery(null);
		return pickupPoint;
	}
	
	public ApiPickupPoint mapToApi (PickupPoint pickupPoint) {
		if(pickupPoint == null) {
			return null;
		}
		var apiPickupPoint = new ApiPickupPoint();
		apiPickupPoint.setAddress(addressMapper.mapToApi(pickupPoint.getAddress()));
		apiPickupPoint.setId(pickupPoint.getId());
		apiPickupPoint.setName(pickupPoint.getName());
		return apiPickupPoint;
	}
	
	public Collection<PickupPoint> mapToDb (Collection<ApiPickupPoint> apiPickupPoints) {
		return apiPickupPoints.stream().map(this::mapToDb).collect(Collectors.toList());
	}
	
	public Collection<ApiPickupPoint> mapToApi (Collection<PickupPoint> pickupPoints) {
		return pickupPoints.stream().map(this::mapToApi).collect(Collectors.toList());
	}
}

package cz.ifoodorder.restaurantservice.api.pojo;

import java.util.UUID;

import lombok.Data;

@Data
public class ApiBitcoin {
	private UUID id;
	private boolean enabled;
	private String address;
}

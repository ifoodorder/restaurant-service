package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiCardOnDelivery;
import cz.ifoodorder.restaurantservice.db.pojo.CardOnDelivery;

@Component
public class CardOnDeliveryMapper {
	private final CardTypeMapper cardTypeMapper;
	public CardOnDeliveryMapper(CardTypeMapper cardTypeMapper) {
		this.cardTypeMapper = cardTypeMapper;
	}
	public CardOnDelivery mapToDb(ApiCardOnDelivery apiCardOnDelivery) {
		if(apiCardOnDelivery == null) {
			return null;
		}
		var cardOnDelivery = new CardOnDelivery();
		cardOnDelivery.setAllowUnregistered(apiCardOnDelivery.isAllowUnregistered());
		cardOnDelivery.setCards(cardTypeMapper.mapToDb(apiCardOnDelivery.getCards()));
		cardOnDelivery.setEnabled(apiCardOnDelivery.isEnabled());
		cardOnDelivery.setId(apiCardOnDelivery.getId());
		return cardOnDelivery;
	}
	public ApiCardOnDelivery mapToApi(CardOnDelivery cardOnDelivery) {
		if(cardOnDelivery == null) {
			return null;
		}
		var apiCardOnDelivery = new ApiCardOnDelivery();
		apiCardOnDelivery.setAllowUnregistered(cardOnDelivery.isAllowUnregistered());
		apiCardOnDelivery.setCards(cardTypeMapper.mapToApi(cardOnDelivery.getCards()));
		apiCardOnDelivery.setEnabled(cardOnDelivery.isEnabled());
		apiCardOnDelivery.setId(cardOnDelivery.getId());
		return apiCardOnDelivery;
	}
}

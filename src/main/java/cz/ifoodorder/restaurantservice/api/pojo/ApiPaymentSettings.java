package cz.ifoodorder.restaurantservice.api.pojo;

import java.util.UUID;

import lombok.Data;

@Data
public class ApiPaymentSettings {
	private UUID id;
	private ApiCash cash;
	private ApiBankTransfer bankTransfer;
	private ApiCardOnDelivery cardOnDelivery;
	private ApiCardOnline cardOnline;
	private ApiBitcoin bitcoin;
	private ApiMealVoucher mealVoucher;
}

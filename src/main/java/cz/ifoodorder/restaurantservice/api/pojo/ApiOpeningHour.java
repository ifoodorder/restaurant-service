package cz.ifoodorder.restaurantservice.api.pojo;

import java.time.LocalTime;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiOpeningHour {
	private UUID id;
	private boolean enabled;
	private LocalTime from;
	private LocalTime to;
	private ApiDay day;
}

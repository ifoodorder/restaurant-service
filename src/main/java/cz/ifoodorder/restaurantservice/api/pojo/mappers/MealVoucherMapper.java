package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiMealVoucher;
import cz.ifoodorder.restaurantservice.db.pojo.MealVoucher;

@Component
public class MealVoucherMapper {
	public MealVoucher mapToDb (ApiMealVoucher apiMealVoucher) {
		if(apiMealVoucher == null) {
			return null;
		}
		var mealVoucher = new MealVoucher();
		mealVoucher.setEnabled(apiMealVoucher.isEnabled());
		mealVoucher.setId(apiMealVoucher.getId());
		return mealVoucher;
	}
	
	public ApiMealVoucher mapToApi (MealVoucher mealVoucher) {
		if(mealVoucher == null) {
			return null;
		}
		var apiMealVoucher = new ApiMealVoucher();
		apiMealVoucher.setEnabled(mealVoucher.isEnabled());
		apiMealVoucher.setId(mealVoucher.getId());
		return apiMealVoucher;
	}
}

package cz.ifoodorder.restaurantservice.api.pojo;

import java.util.UUID;

import lombok.Data;

@Data
public class ApiBankTransfer {
	private UUID id;
	private String bankAccountNumber;
	private String additionalInfo;
	private boolean allowUnregistered;
}

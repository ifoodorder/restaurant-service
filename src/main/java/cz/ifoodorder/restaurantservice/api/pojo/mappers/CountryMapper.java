package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiCountry;
import cz.ifoodorder.restaurantservice.db.dto.CountryDto;
import cz.ifoodorder.restaurantservice.db.pojo.Country;

@Component
public class CountryMapper {
	private CountryDto dto;
	public CountryMapper(CountryDto dto) {
		this.dto = dto;
	}

	public Country mapToDb(ApiCountry viewCountry) {
		if(viewCountry == null || viewCountry.getName().isBlank()) {
			return null;
		}
		return dto.findOneByName(viewCountry.getName()).orElseThrow();
	}

	public ApiCountry mapToApi(Country country) {
		if(country == null) {
			return null;
		}
		var apiCountry = new ApiCountry();
		apiCountry.setName(country.getName());
		return apiCountry;
	}

	public Collection<ApiCountry> mapToApi(Collection<Country> countries) {
		return countries.stream().map(this::mapToApi).collect(Collectors.toList());
	}
}

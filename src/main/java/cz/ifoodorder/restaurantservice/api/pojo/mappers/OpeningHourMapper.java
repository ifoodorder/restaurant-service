package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiOpeningHour;
import cz.ifoodorder.restaurantservice.db.pojo.OpeningHour;
import cz.ifoodorder.restaurantservice.db.pojo.OpeningHoursSettings;

@Component
public class OpeningHourMapper {
	private final DayMapper dayMapper;
	public OpeningHourMapper(DayMapper dayMapper) {
		this.dayMapper = dayMapper;
	}

	public OpeningHour mapToDb(ApiOpeningHour apiOpeningHour) {
		return this.mapToDb(apiOpeningHour, null);
	}
	public OpeningHour mapToDb(ApiOpeningHour apiOpeningHour, OpeningHoursSettings parent) {
		if(apiOpeningHour == null) {
			return null;
		}
		var openingHours = new OpeningHour();
		openingHours.setId(apiOpeningHour.getId());
		openingHours.setDay(dayMapper.mapToDb(apiOpeningHour.getDay()));
		openingHours.setEnabled(apiOpeningHour.isEnabled());
		openingHours.setFrom(apiOpeningHour.getFrom());
		openingHours.setTo(apiOpeningHour.getTo());
		openingHours.setOpeningHoursSettings(parent);
		return openingHours;
	}

	public ApiOpeningHour mapToApi(OpeningHour openingHour) {
		if (openingHour == null) {
			return null;
		}
		var apiOpeningHour = new ApiOpeningHour();
		apiOpeningHour.setDay(dayMapper.mapToApi(openingHour.getDay()));
		apiOpeningHour.setEnabled(openingHour.isEnabled());
		apiOpeningHour.setFrom(openingHour.getFrom());
		apiOpeningHour.setTo(openingHour.getTo());
		apiOpeningHour.setId(openingHour.getId());
		return apiOpeningHour;
	}

	public Collection<ApiOpeningHour> mapToApi(Collection<OpeningHour> openingHours) {
		return openingHours.stream().map(this::mapToApi).collect(Collectors.toList());
	}

	public Collection<OpeningHour> mapToDb(Collection<ApiOpeningHour> apiOpeningHours) {
		return mapToDb(apiOpeningHours, null);
	}
	
	public Collection<OpeningHour> mapToDb(Collection<ApiOpeningHour> apiOpeningHours, OpeningHoursSettings parent) {
		if(apiOpeningHours == null) {
			return Collections.emptyList();
		}
		return apiOpeningHours.stream().map(x -> mapToDb(x, parent)).collect(Collectors.toList());
	}
}

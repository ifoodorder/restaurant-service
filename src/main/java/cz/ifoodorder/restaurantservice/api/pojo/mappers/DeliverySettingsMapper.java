package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiDeliverySettings;
import cz.ifoodorder.restaurantservice.db.pojo.DeliverySettings;

@Component
public class DeliverySettingsMapper {
	private final CourierDeliveryMapper courierDeliveryMapper;
	private final PickupPointDeliveryMapper pickupPointDeliveryMapper;
	private final RestaurantDeliveryMapper restaurantDeliveryMapper;
	
	public DeliverySettingsMapper(PolygonMapper polygonMapper, FeeTypeMapper feeTypeMapper, CourierDeliveryMapper apiCourierDeliveryMapper, RestaurantDeliveryMapper restaurantDeliveryMapper, PickupPointDeliveryMapper pickupPointDeliveryMapper) {
		this.courierDeliveryMapper = apiCourierDeliveryMapper;
		this.pickupPointDeliveryMapper = pickupPointDeliveryMapper;
		this.restaurantDeliveryMapper = restaurantDeliveryMapper;
	}
	
	public DeliverySettings mapToDb(ApiDeliverySettings viewCountry) {
		if(viewCountry == null) {
			return null;
		}
		var deliverySettings = new DeliverySettings();
		deliverySettings.setId(viewCountry.getId());
		deliverySettings.setCourierDelivery(courierDeliveryMapper.mapToDb(viewCountry.getCourierDelivery()));
		deliverySettings.setPickupPointDelivery(pickupPointDeliveryMapper.mapToDb(viewCountry.getPickupPointDelivery()));
		deliverySettings.setRestaurantDelivery(restaurantDeliveryMapper.mapToDb(viewCountry.getRestaurantDelivery()));
		return deliverySettings;
	}
	
	public ApiDeliverySettings mapToApi(DeliverySettings country) {
		if(country == null) {
			return null;
		}
		var deliverySettings = new ApiDeliverySettings();
		deliverySettings.setId(country.getId());
		deliverySettings.setCourierDelivery(courierDeliveryMapper.mapToApi(country.getCourierDelivery()));
		deliverySettings.setPickupPointDelivery(pickupPointDeliveryMapper.mapToApi(country.getPickupPointDelivery()));
		deliverySettings.setRestaurantDelivery(restaurantDeliveryMapper.mapToApi(country.getRestaurantDelivery()));
		return deliverySettings;
	}
	
	public Collection<ApiDeliverySettings> mapToApi(Collection<DeliverySettings> countries) {
		return countries.stream().map(this::mapToApi).collect(Collectors.toList());
	}
}

package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiPaymentSettings;
import cz.ifoodorder.restaurantservice.db.pojo.PaymentSettings;

@Component
public class PaymentSettingsMapper {
	private final CashMapper cashMapper;
	private final BankTransferMapper bankTransferMapper;
	private final CardOnDeliveryMapper cardOnDeliveryMapper;
	private final CardOnlineMapper cardOnlineMapper;
	private final BitcoinMapper bitcoinMapper;
	private final MealVoucherMapper mealVoucherMapper;
	
	
	public PaymentSettingsMapper(CashMapper cashMapper, BankTransferMapper bankTransferMapper,
			CardOnDeliveryMapper cardOnDeliveryMapper, CardOnlineMapper cardOnlineMapper, BitcoinMapper bitcoinMapper,
			MealVoucherMapper mealVoucherMapper) {
		this.cashMapper = cashMapper;
		this.bankTransferMapper = bankTransferMapper;
		this.cardOnDeliveryMapper = cardOnDeliveryMapper;
		this.cardOnlineMapper = cardOnlineMapper;
		this.bitcoinMapper = bitcoinMapper;
		this.mealVoucherMapper = mealVoucherMapper;
	}
	
	public PaymentSettings mapToDb(ApiPaymentSettings apiPaymentSettings) {
		if(apiPaymentSettings == null) {
			return null;
		}
		var paymentSettings = new PaymentSettings();
		paymentSettings.setId(apiPaymentSettings.getId());
		paymentSettings.setBankTransfer(bankTransferMapper.mapToDb(apiPaymentSettings.getBankTransfer()));
		paymentSettings.setBitcoin(bitcoinMapper.mapToDb(apiPaymentSettings.getBitcoin()));
		paymentSettings.setCardOnDelivery(cardOnDeliveryMapper.mapToDb(apiPaymentSettings.getCardOnDelivery()));
		paymentSettings.setCardOnline(cardOnlineMapper.mapToDb(apiPaymentSettings.getCardOnline()));
		paymentSettings.setCash(cashMapper.mapToDb(apiPaymentSettings.getCash()));
		paymentSettings.setMealVoucher(mealVoucherMapper.mapToDb(apiPaymentSettings.getMealVoucher()));
		return paymentSettings;
	}
	
	public ApiPaymentSettings mapToApi(PaymentSettings paymentSettings) {
		if(paymentSettings == null) {
			return null;
		}
		var apiPaymentSettings = new ApiPaymentSettings();
		apiPaymentSettings.setId(paymentSettings.getId());
		apiPaymentSettings.setBankTransfer(bankTransferMapper.mapToApi(paymentSettings.getBankTransfer()));
		apiPaymentSettings.setBitcoin(bitcoinMapper.mapToApi(paymentSettings.getBitcoin()));
		apiPaymentSettings.setCardOnDelivery(cardOnDeliveryMapper.mapToApi(paymentSettings.getCardOnDelivery()));
		apiPaymentSettings.setCardOnline(cardOnlineMapper.mapToApi(paymentSettings.getCardOnline()));
		apiPaymentSettings.setCash(cashMapper.mapToApi(paymentSettings.getCash()));
		apiPaymentSettings.setMealVoucher(mealVoucherMapper.mapToApi(paymentSettings.getMealVoucher()));
		return apiPaymentSettings;
	}
	
	public Collection<ApiPaymentSettings> mapToApi(Collection<PaymentSettings> paymentSettings) {
		return paymentSettings.stream().map(this::mapToApi).collect(Collectors.toList());
	}
	
	public Collection<PaymentSettings> mapToDb(Collection<ApiPaymentSettings> apiPaymentSettings) {
		return apiPaymentSettings.stream().map(this::mapToDb).collect(Collectors.toList());
	}
}

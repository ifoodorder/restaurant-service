package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiPickupPointDelivery;
import cz.ifoodorder.restaurantservice.db.pojo.PickupPointDelivery;

@Component
public class PickupPointDeliveryMapper {
	private final FeeMapper feeMapper;
	private final PickupPointMapper pickupPointMapper;
	public PickupPointDeliveryMapper(FeeMapper feeMapper, PickupPointMapper pickupPointMapper) {
		this.feeMapper = feeMapper;
		this.pickupPointMapper = pickupPointMapper;
	}
	
	public PickupPointDelivery mapToDb(ApiPickupPointDelivery apiPickupPointDelivery) {
		if(apiPickupPointDelivery == null) {
			return null;
		}
		var pickupPointDelivery = new PickupPointDelivery();
		pickupPointDelivery.setId(apiPickupPointDelivery.getId());
		pickupPointDelivery.setEnabled(apiPickupPointDelivery.isEnabled());
		pickupPointDelivery.setFees(feeMapper.mapToDb(apiPickupPointDelivery.getFees()));
		pickupPointDelivery.setNote(apiPickupPointDelivery.getNote());
		pickupPointDelivery.setPickupPoints(pickupPointMapper.mapToDb(apiPickupPointDelivery.getPickupPoints()));
		return pickupPointDelivery;
	}
	
	public ApiPickupPointDelivery mapToApi(PickupPointDelivery pickupPointDelivery) {
		if(pickupPointDelivery == null) {
			return null;
		}
		var apiPickupPointDelivery = new ApiPickupPointDelivery();
		apiPickupPointDelivery.setId(pickupPointDelivery.getId());
		apiPickupPointDelivery.setEnabled(pickupPointDelivery.isEnabled());
		apiPickupPointDelivery.setFees(feeMapper.mapToApi(pickupPointDelivery.getFees()));
		apiPickupPointDelivery.setNote(pickupPointDelivery.getNote());
		apiPickupPointDelivery.setPickupPoints(pickupPointMapper.mapToApi(pickupPointDelivery.getPickupPoints()));
		return apiPickupPointDelivery;
	}
}

package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiCash;
import cz.ifoodorder.restaurantservice.db.pojo.Cash;

@Component
public class CashMapper {
	private final CurrencyMapper currencyMapper;
	public CashMapper(CurrencyMapper currencyMapper) {
		this.currencyMapper = currencyMapper;
	}
	
	public Cash mapToDb(ApiCash apiCash) {
		if(apiCash == null) {
			return null;
		}
		var cash = new Cash();
		cash.setAllowUnregistered(apiCash.isAllowUnregistered());
		cash.setCurrencies(currencyMapper.mapToDb(apiCash.getCurrencies()));
		cash.setEnabled(apiCash.isEnabled());
		cash.setId(apiCash.getId());
		return cash;
	}
	
	public ApiCash mapToApi(Cash cash) {
		if(cash == null) {
			return null;
		}
		var apiCash = new ApiCash();
		apiCash.setAllowUnregistered(cash.isAllowUnregistered());
		apiCash.setCurrencies(currencyMapper.mapToApi(cash.getCurrencies()));
		apiCash.setEnabled(cash.isEnabled());
		apiCash.setId(cash.getId());
		return apiCash;
	}
}

package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiCourierDelivery;
import cz.ifoodorder.restaurantservice.db.pojo.CourierDelivery;

@Component
public class CourierDeliveryMapper {
	private final PolygonMapper polygonMapper;
	
	public CourierDeliveryMapper(PolygonMapper polygonMapper, FeeMapper feeMapper) {
		this.polygonMapper = polygonMapper;
		this.feeMapper = feeMapper;
	}
	private final FeeMapper feeMapper;
	
	public CourierDelivery mapToDb(ApiCourierDelivery viewCourierDelivery) {
		if(viewCourierDelivery == null) {
			return null;
		}
		var courierDelivery = new CourierDelivery();
		courierDelivery.setArea(polygonMapper.mapToDb(viewCourierDelivery.getArea()));
//		TODO:
//		courierDelivery.setDeliveryTimeSettings(deliveryTimeSettingsMapper.mapToDb());
		courierDelivery.setEnabled(viewCourierDelivery.isEnabled());
		courierDelivery.setFees(feeMapper.mapToDb(viewCourierDelivery.getFees()));
		courierDelivery.setId(viewCourierDelivery.getId());
		courierDelivery.setMaxDistance(viewCourierDelivery.getMaxDistance());
		courierDelivery.setNote(viewCourierDelivery.getNote());
		return courierDelivery;
	}
	ApiCourierDelivery mapToApi(CourierDelivery courierDelivery) {
		if(courierDelivery == null) {
			return null;
		}
		var apiCourierDelivery = new ApiCourierDelivery();
		apiCourierDelivery.setArea(polygonMapper.mapToApi(courierDelivery.getArea()));
		apiCourierDelivery.setEnabled(courierDelivery.isEnabled());
		apiCourierDelivery.setFees(feeMapper.mapToApi(courierDelivery.getFees()));
		apiCourierDelivery.setId(courierDelivery.getId());
		apiCourierDelivery.setMaxDistance(courierDelivery.getMaxDistance());
		apiCourierDelivery.setNote(apiCourierDelivery.getNote());
		return apiCourierDelivery;
	}
}

package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiCardType;
import cz.ifoodorder.restaurantservice.db.dto.CardTypeDto;
import cz.ifoodorder.restaurantservice.db.pojo.CardType;

@Component
public class CardTypeMapper {
	private final CardTypeDto dto;
	public CardTypeMapper(CardTypeDto dto) {
		this.dto = dto;
	}
	
	public CardType mapToDb(ApiCardType apiCardType) {
		if(apiCardType == null) {
			return null;
		}
		return dto.findOneByName(apiCardType.getName()).orElseThrow();
	}
	
	public ApiCardType mapToApi(CardType cardType) {
		if(cardType == null) {
			return null;
		}
		return new ApiCardType(cardType.getName());
	}
	
	public Collection<CardType> mapToDb(Collection<ApiCardType> apiCardTypes) {
		return apiCardTypes.stream().map(this::mapToDb).collect(Collectors.toList());		
	}
	
	public Collection<ApiCardType> mapToApi(Collection<CardType> cardTypes) {
		return cardTypes.stream().map(this::mapToApi).collect(Collectors.toList());		
	}
}

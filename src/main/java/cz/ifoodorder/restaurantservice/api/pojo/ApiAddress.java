package cz.ifoodorder.restaurantservice.api.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiAddress {
	private ApiCountry country;
	private String city;
	private String zip;
	private String street;
	private String building;
}

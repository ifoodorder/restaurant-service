package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiFee;
import cz.ifoodorder.restaurantservice.db.pojo.Fee;


@Component
public class FeeMapper {
	private final FeeTypeMapper feeTypeMapper;
	
	public FeeMapper(FeeTypeMapper feeTypeMapper) {
		this.feeTypeMapper = feeTypeMapper;
	}
	
	public Fee mapToDb(ApiFee apiFee) {
		if(apiFee == null) {
			return null;
		}
		var fee = new Fee();
		fee.setEnabled(apiFee.isEnabled());
		fee.setFeeType(feeTypeMapper.mapToDb(apiFee.getFeeType()));
		fee.setFormula(apiFee.getFormula());
		fee.setId(apiFee.getId());
		return fee;
	}
	
	public ApiFee mapToApi(Fee fee) {
		if(fee == null) {
			return null;
		}
		var apiFee = new ApiFee();
		apiFee.setEnabled(fee.isEnabled());
		apiFee.setFeeType(feeTypeMapper.mapToApi(fee.getFeeType()));
		apiFee.setFormula(fee.getFormula());
		apiFee.setId(fee.getId());
		return apiFee;
	}
	
	public Collection<ApiFee> mapToApi(Collection<Fee> fees) {
		return fees.stream().map(this::mapToApi).collect(Collectors.toList());
	}
	public Collection<Fee> mapToDb(Collection<ApiFee> apiFees) {
		return apiFees.stream().map(this::mapToDb).collect(Collectors.toList());
	}
}

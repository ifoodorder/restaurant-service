package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiCurrency;
import cz.ifoodorder.restaurantservice.db.dto.CurrencyDto;
import cz.ifoodorder.restaurantservice.db.pojo.Currency;

@Component
public class CurrencyMapper {
	private final CurrencyDto dto;
	public CurrencyMapper(CurrencyDto dto) {
		this.dto = dto;
	}
	
	public Currency mapToDb(ApiCurrency apiCurrency) {
		if(apiCurrency == null) {
			return null;
		}
		return dto.findOneByName(apiCurrency.getName()).orElseThrow();
	}
	
	public ApiCurrency mapToApi(Currency currency) {
		if(currency == null) {
			return null;
		}
		return new ApiCurrency(currency.getName());
	}
	
	public Collection<ApiCurrency> mapToApi(Collection<Currency> currencies) {
		return currencies.stream().map(this::mapToApi).collect(Collectors.toList());
	}
	
	public Collection<Currency> mapToDb(Collection<ApiCurrency> apiCurrencies) {
		return apiCurrencies.stream().map(this::mapToDb).collect(Collectors.toList());
	}
}

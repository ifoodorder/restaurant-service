package cz.ifoodorder.restaurantservice.api.pojo.mappers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.restaurantservice.api.pojo.ApiDay;
import cz.ifoodorder.restaurantservice.db.dto.DayDto;
import cz.ifoodorder.restaurantservice.db.pojo.Day;


@Component
public class DayMapper {
	private final DayDto dto;
	public DayMapper(DayDto dto) {
		this.dto = dto;
	}
	
	public Day mapToDb(ApiDay apiDay) {
		if(apiDay == null) {
			return null;
		}
		return dto.findOneByName(apiDay.getName()).orElseThrow();
	}
	
	public ApiDay mapToApi(Day day) {
		if(day == null) {
			return null;
		}
		return new ApiDay(day.getName());
	}
	
	public Collection<ApiDay> mapToApi(Collection<Day> days) {
		return days.stream().map(this::mapToApi).collect(Collectors.toList());
	}
}

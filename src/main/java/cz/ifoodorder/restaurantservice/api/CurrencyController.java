package cz.ifoodorder.restaurantservice.api;

import java.util.Collection;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.ifoodorder.restaurantservice.api.pojo.ApiCurrency;
import cz.ifoodorder.restaurantservice.api.pojo.mappers.CurrencyMapper;
import cz.ifoodorder.restaurantservice.db.dto.CurrencyDto;

@RestController
@RequestMapping("/api/restaurants/currencies")
public class CurrencyController {
	private final CurrencyDto currencyDto;
	private final CurrencyMapper currencyMapper;
	
	public CurrencyController(CurrencyDto currencyDto, CurrencyMapper currencyMapper) {
		this.currencyDto = currencyDto;
		this.currencyMapper = currencyMapper;
	}
	
	@GetMapping()
	public Collection<ApiCurrency> getCountries() {
		return currencyMapper.mapToApi(currencyDto.findAll());
	}
}

package cz.ifoodorder.restaurantservice.api;

import java.util.Collection;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cz.ifoodorder.restaurantservice.api.pojo.ApiCountry;
import cz.ifoodorder.restaurantservice.api.pojo.mappers.CountryMapper;
import cz.ifoodorder.restaurantservice.db.dto.CountryDto;
import cz.ifoodorder.restaurantservice.geolocation.GeolocationService;

@RestController
@RequestMapping("/api/restaurants/countries")
public class CountryController {
	private final CountryDto countryDto;
	private final CountryMapper countryMapper;
	private final GeolocationService geolocationService;
	
	public CountryController(CountryDto countryDto, CountryMapper countryMapper, GeolocationService geolocationService) {
		this.countryDto = countryDto;
		this.countryMapper = countryMapper;
		this.geolocationService = geolocationService;
	}
	
	@GetMapping()
	public Collection<ApiCountry> getCountries() {
		return countryMapper.mapToApi(countryDto.findAll());
	}
	
	@GetMapping("/{ipAddress}")
	public ApiCountry findCountryByIp(@PathVariable("ipAddress") String ipAddress) {
		var geo = geolocationService.findByIp(ipAddress);
		var country = countryDto.findOneByName(geo.getCountryCode()).orElseThrow();
		return countryMapper.mapToApi(country);
	}
}

package cz.ifoodorder.restaurantservice.api;

import java.util.UUID;

import org.springframework.security.core.Authentication;

public class UserService {
	
	private UserService() {
		// No constructor
	}
	
	public static boolean hasRole(String role, Authentication authentication) {
		if (authentication == null) {
			return false;
		}
		final boolean hasRole = authentication.getAuthorities().stream().anyMatch(x -> x.getAuthority().equals("ROLE_" + role));
		final boolean hasRoleWithPrefix = authentication.getAuthorities().stream().anyMatch(x -> x.getAuthority().equals(role));
		return hasRole || hasRoleWithPrefix;
	}
	
	public static String getName(Authentication authentication) {
		if (authentication == null) {
			return null;
		}
		return authentication.getName();
	}
	
	public static UUID getUserID(Authentication authentication) {
		if (authentication == null) {
			return null;
		}
		return UUID.fromString(authentication.getName());
	}
}

package cz.ifoodorder.restaurantservice.api;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.security.RolesAllowed;

import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.ifoodorder.restaurantservice.api.pojo.ApiRestaurant;
import cz.ifoodorder.restaurantservice.api.pojo.mappers.RestaurantMapper;
import cz.ifoodorder.restaurantservice.db.dto.CompanyDto;
import cz.ifoodorder.restaurantservice.db.dto.RestaurantDto;
import cz.ifoodorder.restaurantservice.db.dto.RestaurantFilter;
import cz.ifoodorder.restaurantservice.db.dto.RestaurantSpecification;
import cz.ifoodorder.restaurantservice.db.pojo.Restaurant;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/restaurants")
@Slf4j
public class RestaurantsController {
	private final RestaurantDto restaurantDto;
	private final RestaurantMapper restaurantMapper;
	private final CompanyDto companyDto;
	
	public RestaurantsController(
			RestaurantDto restaurantDto, 
			RestaurantMapper restaurantMapper,
			CompanyDto companyDto) {
		this.restaurantDto = restaurantDto;
		this.restaurantMapper = restaurantMapper;
		this.companyDto = companyDto;
	}
	
	@GetMapping()
	public Iterable<ApiRestaurant> getRestaurants(RestaurantFilter restaurantFilter, Authentication authentication) {
		var accessToken = getAccessToken(authentication).orElse(null);
		log.info("Access Token: {}", accessToken);
		var groupId = getGroupId(accessToken).orElse(null);
		log.info("GroupId: {}", groupId);
		RestaurantSpecification rs = new RestaurantSpecification(restaurantFilter, groupId);
		log.info("{}", restaurantFilter);
		List<Restaurant> restaurants = restaurantDto.findAll(rs);
		return restaurantMapper.mapToApi(restaurants);
	}
	
	@GetMapping("/{restaurantId}")
	public Optional<ApiRestaurant> getRestaurant(@PathVariable("restaurantId") UUID restaurantId) {
		log.info("RestaurantId = {}", restaurantId);
		Optional<Restaurant> restaurant = restaurantDto.findById(restaurantId);
		return restaurant.map(restaurantMapper::mapToApi);
	}
	
	@RolesAllowed(value = "ROLE_restaurant-admin")
	@PostMapping()
	public ApiRestaurant postRestaurant (@RequestBody ApiRestaurant viewRrestaurant, Authentication authentication) {
		var accessToken = getAccessToken(authentication).orElseThrow();
		var groupId = getGroupId(accessToken).orElseThrow();
		log.info("[postRestaurant] groupId: {}", groupId);
		var company = companyDto.findById(groupId).orElseThrow();
		
		var restaurant = restaurantMapper.mapToDb(viewRrestaurant);
		restaurant.setCompany(company);
		
		Restaurant restaurantToReturn;
		restaurantToReturn = restaurantDto.save(restaurant);
		return restaurantMapper.mapToApi(restaurantToReturn);
	}
	
	@RolesAllowed(value = "ROLE_restaurant-admin")
	@PutMapping("/{restaurantId}")
	public void putRestaurant(@RequestBody ApiRestaurant viewRestaurant, Authentication authentication) {
		var accessToken = getAccessToken(authentication).orElseThrow();
		var userId = UUID.fromString(authentication.getName());
		var groupId = getGroupId(accessToken).orElseThrow();
		var incomingRestaurant = restaurantMapper.mapToDb(viewRestaurant);
		
		log.debug("User ID: {}", userId);
		log.debug("Restaurant ID: {}", incomingRestaurant.getId());
		
		var oldRestaurant = restaurantDto.findOneByIdAndCompanyId(incomingRestaurant.getId(), groupId).orElseThrow();
		incomingRestaurant.setId(oldRestaurant.getId());
		
		if (incomingRestaurant.getCompany() == null) {
			incomingRestaurant.setCompany(oldRestaurant.getCompany());
		}
		if (incomingRestaurant.getOpeningHoursSettings() != null) {
			incomingRestaurant.getOpeningHoursSettings().setOpeningHours(incomingRestaurant.getOpeningHoursSettings().getOpeningHours().stream()
					.filter(x -> x.getFrom() != null || x.getTo() != null)
					.collect(Collectors.toList()));
		}
		restaurantDto.save(incomingRestaurant);
	}
	
	private Optional<AccessToken> getAccessToken(Authentication auth) {
		return Optional.ofNullable(((KeycloakAuthenticationToken) auth))
				.map(x -> (RefreshableKeycloakSecurityContext)x.getCredentials())
				.map(RefreshableKeycloakSecurityContext::getToken);
	}
	
	@SuppressWarnings("unchecked")
	private Optional<UUID> getGroupId(AccessToken accessToken) {
		return Optional.ofNullable(accessToken)
		.map(AccessToken::getOtherClaims)
		.map(x -> x.get("groups"))
		.map(x -> (List<String>)x)
		.map(x -> x.get(0))
		.map(x -> x.replace("/", ""))
		.map(UUID::fromString);
	}
}

DROP TABLE IF EXISTS language, "card", delivery_time_settings, delivery_time_offset, preorder_constraint, fee, "user_role_xref", "user_restaurant_xref", "role_permission_xref", "role", "user", "permission", "card_online", "bitcoin", "bank_transfer", "card_on_delivery_card_xref", "card_on_delivery", "meal_voucher", "currency_cash_xref", "cash", "payment_settings", "currency", "day", "opening_hours_settings", "courier_delivery_fee_xref", "courier_delivery", "pickup_point", "pickup_point_delivery_fee_xref", "pickup_point_delivery", "restaurant_delivery_fee_xref", "restaurant_delivery", company, country, restaurant_delivery_settings_xref, delivery_settings, fee_type, address, restaurant, restaurant_opening_hours_xref, opening_hours, day_of_week, time_format, calendar_type CASCADE;

create table "company" (
	id UUID primary key not null default uuid_generate_v4(),
	address_id UUID, -- not null,
	company_name text, -- not null,
	legal_entity_number text,
	tax_id text
);

create table "restaurant" (
	id UUID primary key not null default uuid_generate_v4(),
	address_id UUID, -- not null,
	name text,
	logo text,
	note text,
	visible boolean not null default false,
	language_id UUID, -- not null,
	currency_id UUID, -- not null,
	delivery_settings_id UUID, -- not null,
	opening_hours_settings_id UUID, -- not null,
	company_id UUID, -- not null,
	payment_settings_id UUID -- not null
);

create table "address" (
	id UUID primary key not null default uuid_generate_v4(),
	street text, -- not null,
	zip text, -- not null,
	city text, -- not null,
	country_id UUID -- not null
);

create table "country" (
	id UUID primary key not null default uuid_generate_v4(),
	"name" text not null
);

create table "language" (
	id UUID primary key not null default uuid_generate_v4(),
	"name" text not null
);

create table "delivery_settings" (
	id UUID primary key not null default uuid_generate_v4(),
	restaurant_delivery_id UUID,
	pickup_point_delivery_id UUID,
	courier_delivery_id UUID
);

create table "restaurant_delivery" (
	id UUID primary key not null default uuid_generate_v4(),
	enabled boolean not null,
	note text,
	delivery_time_settings_id UUID
);

create table "restaurant_delivery_fee_xref" (
	id UUID primary key not null default uuid_generate_v4(),
	restaurant_delivery_id UUID not null,
	fee_id UUID not null
);

create table "pickup_point_delivery" (
	id UUID primary key not null default uuid_generate_v4(),
	enabled boolean,
	note text,
	delivery_time_settings_id UUID
);

create table "pickup_point_delivery_fee_xref" (
	id UUID primary key not null default uuid_generate_v4(),
	pickup_point_delivery_id UUID not null,
	fee_id UUID not null
);

create table "pickup_point" (
	id UUID primary key not null default uuid_generate_v4(),
	"name" text not null,
	address_id UUID not null,
	pickup_point_delivery_id UUID not null 
);

create table "courier_delivery" (
	id UUID primary key not null default uuid_generate_v4(),
	enabled boolean,
	note text,
	fee_id UUID,
	area bytea, --polygon,
	max_distance numeric,
	delivery_time_settings_id UUID
);

create table "courier_delivery_fee_xref" (
	id UUID primary key not null default uuid_generate_v4(),
	courier_delivery_id UUID not null,
	fee_id UUID not null
);

create table "opening_hours_settings" (
	id UUID primary key not null default uuid_generate_v4(),
	note text
);

create table "opening_hours" (
	id UUID primary key not null default uuid_generate_v4(),
	enabled boolean not null,
	"from" time not null,
	"to" time not null,
	day_id UUID not null,
	opening_hours_settings_id UUID not null
);

create table "day" (
	id UUID primary key not null default uuid_generate_v4(),
	"name" text not null
);

create table "currency" (
	id UUID primary key not null default uuid_generate_v4(),
	"name" text not null
);

create table "payment_settings" (
	id UUID primary key not null default uuid_generate_v4(),
	cash_id UUID,
	bank_transfer_id UUID,
	card_online_id UUID,
	card_on_delivery_id UUID,
	meal_voucher_id UUID,
	bitcoin_id UUID
);

create table "cash" (
	id UUID primary key not null default uuid_generate_v4(),
	enabled boolean not null default false,
	allow_unregistered boolean not null default true
);

create table "currency_cash_xref" (
	id UUID primary key not null default uuid_generate_v4(),
	cash_id UUID not null,
	currency_id UUID not null
);

create table "meal_voucher" (
	id UUID primary key not null default uuid_generate_v4(),
	enabled boolean,
	allow_unregistered boolean
);

create table "card_on_delivery" (
	id UUID primary key not null default uuid_generate_v4(),
	enabled boolean,
	allow_unregistered boolean
);

create table "card" (
	id UUID primary key not null default uuid_generate_v4(),
	name text
);

create table "card_on_delivery_card_xref" (
	id UUID primary key not null default uuid_generate_v4(),
	card_on_delivery_id UUID not null,
	card_id UUID not null
);

create table "bank_transfer" (
	id UUID primary key not null default uuid_generate_v4(),
	bank_account_number text not null,
	additional_info text,
	allow_unregistered boolean not null default true
);

create table "card_online" (
	id UUID primary key not null default uuid_generate_v4(),
	enabled boolean not null default false
);

create table "bitcoin" (
	id UUID primary key not null default uuid_generate_v4(),
	enabled boolean not null default false,
	address text not null
);

create table "fee" (
	id UUID primary key not null default uuid_generate_v4(),
	fee_type_id UUID, -- not null,
	formula text not null,
	enabled boolean
);

create table "fee_type" (
	id UUID primary key not null default uuid_generate_v4(),
	name text not null
);

create table "delivery_time_settings" (
	id UUID primary key not null default uuid_generate_v4(),
	preorder_constraint_id UUID,
	delivery_time_offset_id UUID
);

create table "delivery_time_offset" (
	id UUID primary key not null default uuid_generate_v4(),
	time_offset interval not null,
	formula text,
	enabled boolean
);

create table "preorder_constraint" (
	id UUID primary key not null default uuid_generate_v4(),
	enabled boolean,
	available_after interval,
	available_before interval,
	available_interval interval
);


-- restaurant FK
ALTER TABLE "restaurant" ADD FOREIGN KEY (company_id) REFERENCES "company"(id);
ALTER TABLE "restaurant" ADD FOREIGN KEY (address_id) REFERENCES "address"(id);
ALTER TABLE "restaurant" ADD FOREIGN KEY (language_id) REFERENCES "language"(id);
ALTER TABLE "restaurant" ADD FOREIGN KEY (currency_id) REFERENCES "currency"(id);
ALTER TABLE "restaurant" ADD FOREIGN KEY (delivery_settings_id) REFERENCES "delivery_settings"(id);
ALTER TABLE "restaurant" ADD FOREIGN KEY (opening_hours_settings_id) REFERENCES "opening_hours_settings"(id);
ALTER TABLE "restaurant" ADD FOREIGN KEY (payment_settings_id) REFERENCES "payment_settings"(id);

-- address FK
ALTER TABLE "address" ADD FOREIGN KEY (country_id) REFERENCES country(id);

-- opening_hour FK
ALTER TABLE "opening_hours" ADD FOREIGN KEY (opening_hours_settings_id) REFERENCES "opening_hours_settings"(id);

-- payment_settings FK
ALTER TABLE "payment_settings" ADD FOREIGN KEY (cash_id) REFERENCES "cash"(id);
ALTER TABLE "payment_settings" ADD FOREIGN KEY (bank_transfer_id) REFERENCES "bank_transfer"(id);
ALTER TABLE "payment_settings" ADD FOREIGN KEY (card_online_id) REFERENCES "card_online"(id);
ALTER TABLE "payment_settings" ADD FOREIGN KEY (card_on_delivery_id) REFERENCES "card_on_delivery"(id);
ALTER TABLE "payment_settings" ADD FOREIGN KEY (meal_voucher_id) REFERENCES "meal_voucher"(id);
ALTER TABLE "payment_settings" ADD FOREIGN KEY (bitcoin_id) REFERENCES "bitcoin"(id);

-- delivery_settings FK
ALTER TABLE "delivery_settings" ADD FOREIGN KEY (restaurant_delivery_id) REFERENCES "restaurant_delivery"(id);
ALTER TABLE "delivery_settings" ADD FOREIGN KEY (pickup_point_delivery_id) REFERENCES "pickup_point_delivery"(id);
ALTER TABLE "delivery_settings" ADD FOREIGN KEY (courier_delivery_id) REFERENCES "courier_delivery"(id);

-- opening_hours FK
ALTER TABLE "opening_hours" ADD FOREIGN KEY (day_id) REFERENCES "day"(id);

-- card_on_delivery_card_xref FK
ALTER TABLE "card_on_delivery_card_xref" ADD FOREIGN KEY (card_id) REFERENCES "card"(id);
ALTER TABLE "card_on_delivery_card_xref" ADD FOREIGN KEY (card_on_delivery_id) REFERENCES "card_on_delivery"(id);

-- currency_cash_xref FK
ALTER TABLE "currency_cash_xref" ADD FOREIGN KEY (currency_id) REFERENCES "currency"(id);
ALTER TABLE "currency_cash_xref" ADD FOREIGN KEY (cash_id) REFERENCES "cash"(id);

-- pickup_point_delivery_fee_xref FK
ALTER TABLE "pickup_point_delivery_fee_xref" ADD FOREIGN KEY (pickup_point_delivery_id) REFERENCES "pickup_point_delivery"(id);
ALTER TABLE "pickup_point_delivery_fee_xref" ADD FOREIGN KEY (fee_id) REFERENCES "fee"(id);

-- restaurant_delivery_fee_xref FK
ALTER TABLE "restaurant_delivery_fee_xref" ADD FOREIGN KEY (restaurant_delivery_id) REFERENCES "restaurant_delivery"(id);
ALTER TABLE "restaurant_delivery_fee_xref" ADD FOREIGN KEY (fee_id) REFERENCES "fee"(id);

-- courier_delivery_fee_xref FK
ALTER TABLE "courier_delivery_fee_xref" ADD FOREIGN KEY (courier_delivery_id) REFERENCES "courier_delivery"(id);
ALTER TABLE "courier_delivery_fee_xref" ADD FOREIGN KEY (fee_id) REFERENCES "fee"(id);

-- pickup_point FK
ALTER TABLE "pickup_point" ADD FOREIGN KEY (address_id) REFERENCES "address"(id);
ALTER TABLE "pickup_point" ADD FOREIGN KEY (pickup_point_delivery_id) REFERENCES "pickup_point_delivery"(id);

-- delivery_time_settings FK
ALTER TABLE "delivery_time_settings" ADD FOREIGN KEY (delivery_time_offset_id) REFERENCES "delivery_time_offset"(id);
ALTER TABLE "delivery_time_settings" ADD FOREIGN KEY (preorder_constraint_id) REFERENCES "preorder_constraint"(id);

-- courier_delivery FK
ALTER TABLE "courier_delivery" ADD FOREIGN KEY (delivery_time_settings_id) REFERENCES "delivery_time_settings"(id);

-- restaurant_delivery FK
ALTER TABLE "restaurant_delivery" ADD FOREIGN KEY (delivery_time_settings_id) REFERENCES "delivery_time_settings"(id);


-- pickup_point_delivery FK
ALTER TABLE "pickup_point_delivery" ADD FOREIGN KEY (delivery_time_settings_id) REFERENCES "delivery_time_settings"(id);

-- fee_fee_type FK
ALTER TABLE "fee" ADD FOREIGN KEY (fee_type_id) REFERENCES "fee_type"(id);
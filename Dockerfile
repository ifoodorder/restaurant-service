FROM adoptopenjdk/openjdk11:alpine-jre
RUN addgroup -S ifoodorder \
    && adduser -S ifoodorder -G ifoodorder \
    && apk update \
    && apk add nginx openssl bash \
    && mkdir /etc/nginx/templates/

COPY docker/service.conf.tpl /etc/nginx/templates/service.conf.tpl
COPY docker/docker-entrypoint.sh /docker-entrypoint.sh

RUN chmod +x docker-entrypoint.sh

#USER ifoodorder:ifoodorder
COPY target/*.jar app.jar
ENV HTTP_PORT 80
ENV HTTPS_PORT 443
ENV RESTAURANT_HOST http://restaurant-service:8050
ENV MENU_HOST http://menu-service:8053
ENV KEYCLOAK_SECRET 98585d22-4e4f-4e1c-9122-9890be29bbde
ENV KEYCLOAK_RESOURCE restaurant-service
ENV KEYCLOAK_URL http://keycloak:8080/auth
ENV JDBC_URL jdbc:postgresql://localhost:5432/restaurant-service

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/docker-entrypoint.sh"]
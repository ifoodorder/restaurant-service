variable "ci_user" {
    type = string
}
variable "ci_pass" {
    type = string
}
variable "image" {
    type = string
}
job "restaurant-service" {
  datacenters = ["dc1"]
  type = "service"
  group "restaurant-service" {
    network {
      port "app" {}
    }
    service {
      name = "restaurant-service"
      tags = ["api"]
      port = "app"
    }
    task "restaurant-service" {
      vault {
        policies = ["restaurant-service"]
      }
      resources {
        cpu = 2000
        memory = 2000
      }
      template {
        data = file("nomad/config.env.tpl")
        destination = "secrets/config.env"
        env = true
      }
      template {
        data = file("nomad/key.tpl")
        destination = "secrets/key.pem"
      }
      template {
        data = file("nomad/cert.tpl")
        destination = "local/cert.pem"
      }
      
      driver = "docker"
      config {
        image = var.image
        ports = ["app"]
        auth {
          username = var.ci_user
          password = var.ci_pass
        }
      }
    }
  }
}